#
# Exportamos nuestra key SSH

variable "pub_key" {}

resource "digitalocean_ssh_key" "kleta" {
  name       = "HP ENVY"
  public_key = file(var.pub_key)
}
