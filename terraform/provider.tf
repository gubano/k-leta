terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "1.22.2"
    }
    docker = {
      source = "terraform-providers/docker"
    }
  }
}

variable "do_token" {}

provider "digitalocean" {
  token = var.do_token
}