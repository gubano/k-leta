import axios from "axios";

const ENDPOINT_PATH = "https://my.api.mockaroo.com/{model}.json?key=a0ed8fa0";

export default {
    get(providerId) {
        const parameters = { providerId };
        return axios.get(ENDPOINT_PATH.replace('{model}', 'provider'));
    },

    
};