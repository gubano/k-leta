import axios from "axios";

const ENDPOINT_PATH = "https://reqres.in/api/";

export default {
    login(email, password) {
        const user = { email, password };
        return axios.post(ENDPOINT_PATH + "login", user);
    },

    logout() {        
        return axios.post(ENDPOINT_PATH + "logout");
    },

    setHeaderAuth(token) {
        axios.defaults.headers.common['Authorization'] = token
    },

    removeHeaderAuth() {
        delete axios.defaults.headers.common['Authorization']
    }
};