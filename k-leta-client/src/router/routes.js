
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Map.vue') },
      { path: '/profile', component: () => import('pages/auth/Profile.vue') },
      { path: '/login', component: () => import('pages/auth/Login.vue') },
      { path: '/signup', component: () => import('pages/auth/Signup.vue') },
      { path: '/forget-password', component: () => import('pages/auth/ForgetPassword.vue') },    
      { path: '/me/products', component: () => import('pages/MyProducts.vue'), meta: { requiresAuth: true } },
      { path: '/map', component: () => import('pages/Map.vue') },
    ]
  },



  {
    path: '/provider/product/add',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/provider/AddItemProvider') }
    ]
  },


  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
