/*
export function someAction (context) {
}
*/

export async function findCurrentPosition({ commit }) {
    return new Promise((resolve, reject) => {

        const geolocation = navigator && navigator.geolocation
        if (!geolocation) {
            console.error('Geolocation object not available')
            reject('Geolocation object not available')
        }
        geolocation.getCurrentPosition(
            position => {
                commit('setCurrentPosition', cloneAsObject(position))
                resolve()
            },
            error => {
                console.error(`Failed getting geolocation: ${error.message}`)
                commit('setCurrentPosition', null)
                reject(error)
            })

    })
}

export async function queryPermissionGeolocation({ commit }) {
    return new Promise((resolve, reject) => {

        navigator.permissions.query({ name: 'geolocation' })
            .then((result) => {
                // one of (granted, prompt, denied)                
                commit('setPermissionGeolocation', result.state)
                resolve()
            })
            .catch(error => {
                reject(error)
            })
    })
}

export async function setGeosearchLocation({ commit }, { location }) {
    return new Promise((resolve, reject) => {
        var position = {
            coords: {
                latitude: location.y,
                longitude: location.x,                
            },
            timestamp: new Date(),
        }

        commit('setCurrentPosition', position)
        resolve()
    })
}

function cloneAsObject(obj) {
    if (obj === null || !(obj instanceof Object)) {
        return obj
    }
    const temp = {}
    for (var key in obj) {
        temp[key] = cloneAsObject(obj[key])
    }
    return temp
}