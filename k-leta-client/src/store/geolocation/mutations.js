/*
export function someMutation (state) {
}
*/
export function setCurrentPosition(state, position) {
    state.position = position
}

export function setPermissionGeolocation(state, permission) {
    state.permission = permission
}