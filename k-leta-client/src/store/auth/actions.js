/*
export function someAction (context) {
}
*/
import { getCurrentUserId, } from 'src/utils/auth'
import AuthFactory from 'src/factories/auth'

export async function fetchCurrentUser({ commit, dispatch, state, rootState, rootGetters }, { forceRefresh = false } = {}) {
    // only fetch current user data in browser environment
    // because the authentication tokens are stored in local storage
    // so they aren't available in server-side
    const isBrowser = typeof window === 'object'
    if (!isBrowser) return

    const currentUser = state.user || {}
    const userId = getCurrentUserId()

    if (!forceRefresh && currentUser.id === userId) return currentUser

    if (userId) {
        commit({
            type: types.SET_CURRENT_USER,
            user: currentUser
        })
        try {
            const user = await stelace.users.read(userId)

            commit({
                type: types.SET_CURRENT_USER,
                user
            })
        } catch (err) {
            commit({
                type: types.SET_CURRENT_USER,
                user: null
            })

            // the current user no longer exists, so we remove the auth token
            if (err.statusCode === 404) {
                const tokenStore = stelace.getTokenStore()
                tokenStore.removeTokens()
            } else {
                throw err
            }
        }
    } else {
        commit({
            type: types.SET_CURRENT_USER,
            user: null
        })
    }
}

export async function login({ commit }, { username, password }) {
    return new Promise((resolve, reject) => {
        commit('authRequest')
        AuthFactory.login(username, password)
            .then(resp => {
                const token = resp.data.token
                const user = resp.data.user
                localStorage.setItem('token', token)
                AuthFactory.setHeaderAuth(token);
                commit('authSuccess', token, user)                
                resolve(resp)
            })
            .catch(err => {
                commit('authError')
                localStorage.removeItem('token')
                reject(err)
            })
    })
}

export async function logout({ commit }) {
    return new Promise((resolve, reject) => {
        commit('logout')
        AuthFactory.logout()
        localStorage.removeItem('token')
        AuthFactory.removeHeaderAuth();

        resolve()
    })
}