export function isLoggedIn(state) {
    return !!state.token
}

export function authStatus(state) {
    return state.status
}

export function currentUser(state, getters, rootState, rootGetters) {
    const user = Object.assign({}, state.token)

    if (user.id) {
        populateUser(user, { categoriesById, isCurrentUser: true })
    }

    return user
}