const user = {
    avatar: "https://res.cloudinary.com/mirifa/image/upload/v1545101388/avatars/5c185f19a5f7b949cd83c2a3/s1sy4bxo9pyltz2ax98x.jpg",
    name: "Gustavo Barraza",
    userSN: "@gustavo.barraza"
}
import jsonGeopoints from './dataGeoPoints.json'
import jsonItems from './dataItems.json'

let providers = [];

var arrayGeopoint = [];
jsonGeopoints.forEach(element => {
    arrayGeopoint.push({
        name: element.alm_nombre_fantasia,
        phone: "+569 56697233",
        avatar: "https://cdn.quasar.dev/img/boy-avatar.png",
        rating: 0,
        location: {
            commune: {
                id: 1,
                name: element.comuna,
                lat: -23.1007,
                lng: -70.447388,
                region: {
                    id: 1,
                    name: "II Región",
                },
            },
            popup: "El vendedor se encuentra aquí",
            lat: element.position.lat,
            lng: element.position.lng,
        },
        products: jsonItems
    });
});

providers = arrayGeopoint;


export default class MockService {

    getUserMock() {
        return user
    }

    getMyProducts() {
        return providers[0].products
    }

    getProviders() {
        return providers
    }

    getProducts() {
        return providers
    }

}