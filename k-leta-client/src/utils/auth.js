//import stelace from './stelace'
import { get } from 'lodash'

export const userMetadataMapping = {
  categoryId: 'metadata.instant.categoryId',
  avatarUrl: 'metadata.instant.avatarUrl',
  publicName: 'metadata.instant.publicName',
  locations: 'metadata.instant.locations',

  images: 'metadata.images',

  averageRating: 'metadata.instant.averageRating',
  score: 'metadata.instant.score',

  firstName: 'metadata._premium.firstname',
  lastName: 'metadata._premium.lastname',

  availabilityId: 'platformData.instant.availabilityId',

}

let userId = null;
let tokenStore = {
  token: {
    accessToken: null
  }
};

// Use this synchronous function to know if the current user is authenticated
// without a network request
// e.g. to display login modal for unauthenticated users when they arrive in the app
export function getCurrentUserId() {
  const { userId } = this.userId //stelace.auth.info()
  return userId
}

export function getAuthToken() {
  const tokenStore = this.tokenStore //stelace.getTokenStore()
  const tokens = tokenStore.token

  return tokens && tokens.accessToken
}

export function getSSOLoginUrl(provider) {
  const publicPlatformId = process.env.STELACE_PUBLIC_PLATFORM_ID

  return `${process.env.STELACE_API_URL}/auth/sso/${publicPlatformId}/${provider}`
}

export function populateUser(user, {
  categoriesById,
  isCurrentUser
} = {}) {


  user.avatarUrl = get(user, userMetadataMapping.avatarUrl, '')
  user.publicName = get(user, userMetadataMapping.publicName, '')
  user.locations = get(user, userMetadataMapping.locations) || []
  user.locationName = get(user.locations, '[0].shortDisplayName', '')

  user.images = get(user, userMetadataMapping.images, [])

  user.availabilityId = get(user, userMetadataMapping.availabilityId, '')

  user.firstName = user.firstname
  user.lastName = user.lastname
}
