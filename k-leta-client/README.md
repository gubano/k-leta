# k-leta-app (k-leta-client)

A Quasar Framework app

## Install the dependencies
```bash
npm install
```


## Install the dependencies
```bash
npm install -g @quasar/icongenie
```


### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).


## regenerate icons all plataform
```bash
icongenie generate -i \opt\workspace\nodejs\k-leta\k-leta-client\public\icons\logo512x512.png --skip-trim
```
